#include "ARP.hxx"

char*
ARP::setIpT(const char ipT[])
{
  strncpy(this->ipT, ipT, 16);
  int err = inet_pton(AF_INET, this->ipT, this->arpHDR.TPA);
  if (err <= 0) {
    return 0;
  } else {
    this->srcOK = true;
    return this->ipT;
  }
}

char*
ARP::setIpG(const char ipG[])
{
  strncpy(this->ipG, ipG, 16);
  int err = inet_pton(AF_INET, this->ipG, this->arpHDR.SPA);
  if (err <= 0) {
    return 0;
  } else {
    this->dstOK = true;
    return this->ipG;
  }
}

bool
ARP::setInterfata(const char numeInterfata[])
{
  strncpy(this->numeInterfata, numeInterfata, IFNAMSIZ);
  int err = this->getMac(this->numeInterfata);
  if (err < 0) {
    std::cerr << "Problema la GETMAC" << std::endl;
    return false;
  }
  memcpy(this->sockLL.sll_addr, this->ifreqInterfata.ifr_ifru.ifru_addr.sa_data,
         8);
  this->sockLL.sll_ifindex = if_nametoindex(this->numeInterfata);
  this->sockLL.sll_family = AF_PACKET;
  this->sockLL.sll_halen = ARP::HLEN;

  memcpy(this->eth2HDR.SHA, this->ifreqInterfata.ifr_ifru.ifru_addr.sa_data,
         this->HLEN);
  memcpy(this->arpHDR.SHA, this->ifreqInterfata.ifr_ifru.ifru_addr.sa_data,
         this->HLEN);

  this->ifNameOK = true;
  return true;
}

int
ARP::getMac(const char numeInterfata[])
{
  int s = socket(PF_INET, SOCK_DGRAM, 0);
  memset(&this->ifreqInterfata, 0, sizeof this->ifreqInterfata);
  strncpy(this->ifreqInterfata.ifr_ifrn.ifrn_name, numeInterfata, IFNAMSIZ);
  int err = ioctl(s, SIOCGIFHWADDR, &this->ifreqInterfata);
  close(s);
  return err;
}

ARP::ARP()
{

  try {

    if (getuid() != 0) {
      throw "Avem nevoie de root!";
    }
    this->sofd = socket(PF_PACKET, SOCK_RAW, IPPROTO_RAW);
    if (this->sofd < 0) {
      throw "Problema la socket";
    }

    this->init();

  } catch (const char* err) {
    std::cerr << err << std::endl;
    exit(EXIT_FAILURE);
  }
}

ARP::ARP(const char ipT[], const char ipG[], const char numeInterfata[])
{

  try {

    if (getuid() != 0) {
      throw "Avem nevoie de root!";
    }
    this->sofd = socket(PF_PACKET, SOCK_RAW, IPPROTO_RAW);
    if (this->sofd < 0) {
      throw "Problema la socket";
    }

    this->init();

    if (this->setIpT(ipT) == 0) {
      throw "Eroare la IP Target";
    }
    if (this->setIpG(ipG) == 0) {
      throw "Eroare la Ip Gateway";
    }

    if (this->setInterfata(numeInterfata) == false) {
      throw "Eroare la nume interfata";
    }

  } catch (const char* err) {
    std::cerr << err << std::endl;
    exit(EXIT_FAILURE);
  }
}

bool
ARP::check()
{

  bool status = true;

  if (!this->srcOK || !this->dstOK || !this->ifNameOK) {
    status = false;
  }
  memcpy(&this->arpRAW.eth2, &this->eth2HDR, sizeof this->eth2HDR);
  memcpy(&this->arpRAW.arp, &this->arpHDR, sizeof this->arpHDR);

  return status;
}

void
ARP::Send(uint64_t milisecunde)
{
  if (this->check() == false) {
    std::cerr << "Eroare la date" << std::endl;
    return;
  }
  std::chrono::steady_clock::rep timp_total(milisecunde), timp;
  std::chrono::steady_clock::time_point start, end;

  uint64_t count = 0;
  start = std::chrono::steady_clock::now();
  do {

    if (this->send()) {
      count++;
    }

    end = std::chrono::steady_clock::now();
    timp = std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
             .count();

  } while (timp < timp_total);

  std::cout << "Am trimis " << count << " de pachete ¬_¬" << std::endl;
}

bool
ARP::send()
{

  bool sent = true;

  if ((sendto(this->sofd, &this->arpRAW, sizeof this->arpRAW, 0,
              (struct sockaddr*)&this->sockLL, sizeof this->sockLL)) <= 0) {
    sent = false;
    std::cerr << "Eroare la transmitere" << std::endl;
  }
  return sent;
}

void
ARP::printARP()
{
  this->check();

  std::cout << std::endl << std::endl;

  std::cout << "HTYPE: " << std::hex << ntohs(this->arpRAW.arp.HTYPE)
            << std::endl;
  std::cout << "PTYPE: " << std::hex << ntohs(this->arpRAW.arp.PTYPE)
            << std::endl;

  std::cout << "Adresa Mac Emitator:" << std::endl;
  for (int i = 0; i < 6; i++) {
    std::cout << std::hex << static_cast<unsigned>(this->arpRAW.arp.SHA[i])
              << ((i < 5) ? ":" : "\n");
  }
  std::cout << "Adresa IP Emitator:" << std::endl;
  for (int i = 0; i < 4; i++) {
    std::cout << std::dec << static_cast<unsigned>(this->arpRAW.arp.SPA[i])
              << ((i < 3) ? "." : "\n");
  }
  std::cout << "Adresa Mac Receptor:" << std::endl;
  for (int i = 0; i < 6; i++) {
    std::cout << std::hex << static_cast<unsigned>(this->arpRAW.arp.THA[i])
              << ((i < 5) ? ":" : "\n");
  }
  std::cout << "Adresa IP Receptor:" << std::endl;
  for (int i = 0; i < 4; i++) {
    std::cout << std::dec << static_cast<unsigned>(this->arpRAW.arp.TPA[i])
              << ((i < 3) ? "." : "\n");
  }

  std::cout << std::endl << std::endl;
}

ARP::~ARP()
{
  if (this->sofd >= 0) {
    close(this->sofd);
  }
}

void
ARP::init()
{
  this->sockLL = { 0 };
  memset(&this->arpHDR, 0xFF, sizeof this->arpHDR);
  memset(&this->eth2HDR, 0xFF, sizeof this->arpHDR);
  memset(&this->arpRAW, 0xFF, sizeof this->arpHDR);

  this->arpHDR.HLEN = ARP::HLEN;
  this->arpHDR.HTYPE = htons(ARP::HTYPE);
  this->arpHDR.OPER = htons(ARP::OPER_REPLY);
  this->arpHDR.PLEN = ARP::PLEN;
  this->arpHDR.PTYPE = htons(ARP::PTYPE);

  this->eth2HDR.TYPE = htons(ARP::TYPE);
}
