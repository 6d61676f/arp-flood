#ifndef ARP_HXX
#define ARP_HXX

#include <arpa/inet.h>
#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <linux/if_packet.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

struct arpHDR_t
{
  uint16_t HTYPE; // Hardware Type
  uint16_t PTYPE; // Protocol Type
  uint8_t HLEN;   // Hardware Address Length
  uint8_t PLEN;   // Protocol Address Length
  uint16_t OPER;  // Operation
  uint8_t SHA[6]; // Sender Hardware Address
  uint8_t SPA[4]; // Sender Protocol Address
  uint8_t THA[6]; // Target Hardware Address
  uint8_t TPA[4]; // Target Protocol Address
};

struct eth2HDR_t
{
  uint8_t THA[6];
  uint8_t SHA[6];
  uint16_t TYPE;
};

struct arpRAW_t
{
  struct eth2HDR_t eth2;
  struct arpHDR_t arp;
};

class ARP
{
public:
  ARP();
  ARP(const char ipT[16], const char ipG[16],
      const char numeInterfata[IFNAMSIZ]);
  virtual ~ARP();
  static const uint16_t HTYPE = 1;
  static const uint16_t PTYPE = 0x0800;
  static const uint8_t HLEN = 6;
  static const uint8_t PLEN = 4;
  static const uint16_t OPER_REPLY = 2;
  static const uint16_t OPER_REQUEST = 1;
  static const uint16_t TYPE = 0x0806;
  char* setIpT(const char ipT[16]);
  char* setIpG(const char ipG[16]);
  bool setInterfata(const char numeInterfata[IFNAMSIZ]);
  void Send(uint64_t milisecunde);
  void printARP();

private:
  bool send();
  bool check();
  bool srcOK = false;
  bool dstOK = false;
  bool ifNameOK = false;
  int getMac(const char numeInterfata[IFNAMSIZ]);
  void init();
  struct ifreq ifreqInterfata;
  arpHDR_t arpHDR;
  eth2HDR_t eth2HDR;
  arpRAW_t arpRAW;
  struct sockaddr_ll sockLL;
  char ipT[16];
  char ipG[16];
  char numeInterfata[IFNAMSIZ];
  int sofd;
};

#endif /* ARP_HXX */
