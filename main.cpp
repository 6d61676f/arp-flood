#include "ARP.hxx"
#include <climits>

int
main(int argc, char** argv)
{

  if (argc != 4) {
    std::cerr << "Apelare corecta: " << argv[0]
              << " interfata ipTarget ipGateway" << std::endl;
    return EXIT_FAILURE;
  } else {

    ARP p(argv[3], argv[2], argv[1]);
    ARP q(argv[2], argv[3], argv[1]);
    p.printARP();
    q.printARP();

    for (uint64_t i = 0; i < ULONG_MAX; i++) {
      p.Send(10);
      q.Send(10);
    }

    return EXIT_SUCCESS;
  }
}
